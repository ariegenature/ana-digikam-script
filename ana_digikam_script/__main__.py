"""Main application entry point.

::

    python -m ana_digikam_script  ...

"""


def main():
    """Run the application."""
    raise NotImplementedError


if __name__ == "__main__":
    raise SystemExit(main())
