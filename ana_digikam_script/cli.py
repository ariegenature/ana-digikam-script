"""Implementation of the command line interface."""

from argparse import ArgumentParser
from collections import ChainMap
from configparser import ConfigParser
from contextlib import contextmanager
import logging

from ana_digikam_script.__version__ import __version__
from ana_digikam_script.exiftool import find_exiftool
from ana_digikam_script.update_digikam import UpdateDigikam, find_digikam_app_folder
from ana_digikam_script.process import ProcessPicture


__all__ = 'main',


_DEFAULT_CONFIG = {
    'log_level': logging.WARNING,
    'log_format': 'ana[{process}] {asctime} ({levelname}) {message}',
    'log_style': '{',
}


def main() -> int:
    """Execute the application CLI.

    :return: exit status
    """
    with _init_config() as config:
        try:
            find_exiftool(config)
            find_digikam_app_folder(config)
            command = config['command']
            logging.debug('Running command `{command}`...'.format(command=command))
            if command == 'update_digikam':
                update_digikam = UpdateDigikam(config)
                update_digikam()
            elif command == 'process':
                process_picture = ProcessPicture(config)
                process_picture(config['input'][0], config['output'][0])
        except Exception:
            logging.exception('Fatal error: terminating...')
            return 1
        except KeyboardInterrupt:
            logging.info('Terminating as requested...')
            return 1
        return 0


@contextmanager
def _init_config():
    """Context manager initializing configuration and shutting logging down properly on exit."""
    args = _parse_args()
    config = ChainMap(_DEFAULT_CONFIG)
    config = config.new_child(_parse_config_file(args.config))
    config = config.new_child(vars(args))
    logging.basicConfig(**{k[4:]: v for k, v in config.items()
                           if k.startswith('log_') and v is not None})
    logging.debug('Started.')
    yield config
    logging.debug('Terminated.')
    logging.shutdown()


def _parse_args():
    """Parse command line arguments."""
    parser = ArgumentParser()
    parser.add_argument('-v', '--version', action='version',
                        version='ana_digikam_script {:s}'.format(__version__),
                        help='Print version and exit')
    parser.add_argument('-c', '--config', required=True,
                        help='Configuration file')
    parser.add_argument('-l', '--log-level', type=str.upper, default='WARN',
                        choices=('DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL'),
                        help='Logging level [default: warn]')
    parser.add_argument('-t', '--log-filename', default=None,
                        help='Logging file [default: none, log to stderr]')
    subparsers = parser.add_subparsers(dest='command', title='Commands', required=True)
    subparsers.add_parser('update_digikam')
    process_parser = subparsers.add_parser('process')
    process_parser.add_argument('input', nargs=1,
                                help='Input image path [provided by Digikam]')
    process_parser.add_argument('output', nargs=1,
                                help='Output image path [provided by Digikam]')
    return parser.parse_args()


def _parse_config_file(fpath):
    """Parse configuration file."""
    parser = ConfigParser()
    parser.read(fpath)
    return parser['config']


if __name__ == '__main__':
    raise SystemExit(main())
