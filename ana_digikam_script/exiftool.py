"""Common tools to be used by photo management tasks."""

import json
import platform
import re
import shutil
import subprocess

_SPACE_REGEXP = re.compile(r'\s+')
_SYMBOL_REGEXP = re.compile(r'[^\w]+')


EXIFTOOL_PATH = None

_PROPERTY_MAP = {
    'AUTHOR': (
        'XMP:Artist',
        'XMP:Creator',
        'IPTC:By-line',
    ),
    'TITLE': (
        'XMP:Caption',
        'XMP:Title',
        'IPTC:ObjectName',
    ),
    'CAPTION': (
        'File:Comment',
        'EXIF:ImageDescription',
        'EXIF:UserComment',
        'XMP:Description',
        'XMP:ImageDescription',
        'XMP:Notes',
        'XMP:UserComment',
        'IPTC:Caption-Abstract',
    ),
    'TAGS': (
        'XMP:CatalogSets',
        'XMP:HierarchicalSubject',
        'XMP:LastKeywordXMP',
        'XMP:Subject',
        'XMP:TagsList',
        'IPTC:Keywords',
    ),
    'COPYRIGHT': (
        'XMP:Copyright',
        'IPTC:CopyrightNotice',
    ),
}


def find_exiftool(config):
    global EXIFTOOL_PATH
    EXIFTOOL_PATH = config.get('exiftool_path') or shutil.which('exiftool')


def run_exiftool(*exiftool_args):
    """Run exiftool with given options."""
    if EXIFTOOL_PATH is None:
        raise RuntimeError("exiftool not found")
    if platform.system() == 'Windows':
        cmd = (EXIFTOOL_PATH,) + ('-charset', 'Latin') + exiftool_args
    else:
        cmd = (EXIFTOOL_PATH,) + exiftool_args
    return subprocess.run(cmd, capture_output=True, check=True)


def json_metadata(img_path):
    """Return metadata from the given picture in the JSON format."""
    return json.loads(run_exiftool('-sort',
                                   '-veryShort',
                                   '-groupNames',
                                   '-argFormat',
                                   '-n',
                                   '-json',
                                   img_path).stdout.decode('utf-8'))[0]


def has_meta_tag(img_path, metaprop):
    """Check that the given tag exists on the given picture."""
    return all(_has_tag(img_path, prop) for prop in _PROPERTY_MAP[metaprop])


def set_meta_property(img_path, metaprop, value):
    for prop in _PROPERTY_MAP[metaprop]:
        _set_property(img_path, prop, value)


def append_to_meta_property(img_path, metaprop, value, sep=''):
    for prop in _PROPERTY_MAP[metaprop]:
        _append_to_property(img_path, prop, value, sep=sep)


def _has_tag(img_path, prop):
    """Check that the given tag exists on the given picture."""
    return bool(json_metadata(img_path).get(prop))


def _set_property(img_path, prop, value):
    """Add, overwrite, or remove the given property in the given picture metadata.

    If ``value`` is None (or an empty string), the property is removed.
    """
    if value is None or not str(value):
        prop_str = '-{prop}='.format(prop=prop)
    else:
        prop_str = '-{prop}={value}'.format(prop=prop, value=value)
    return run_exiftool(prop_str, img_path)


def _append_to_property(img_path, prop, value, sep=''):
    """Append the given text value to the given property in the given picture medatata.

    ``sep`` is a string to be inserted between the previous value and the value appended.
    """
    md = json_metadata(img_path)
    previous_value = md.get(prop) or ''
    return _set_property(img_path, prop, '{previous}{sep}{new}'.format(previous=previous_value,
                                                                       sep=sep,
                                                                       new=value))
