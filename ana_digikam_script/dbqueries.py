"""Tools to work with tags."""

import difflib
import sqlite3


def parent_tags(tag_name, dbpath, tablename='tags', id_col='id', parent_col='pid', tag_col='name'):
    """Yield each parent tag of the given one."""
    query = """with recursive tag_tree({id_col}, {parent_col}, {tag_col}) as (
        select tags.{id_col}, tags.{parent_col}, tags.{tag_col}
        from {tablename}
        where {tag_col} = :tag_name
      union
        select tags.{id_col}, tags.{parent_col}, tags.{tag_col}
        from {tablename}
        inner join tag_tree as tree on {tablename}.{id_col} = tree.{parent_col}
    )
    select {tag_col}
    from tag_tree
    where {tag_col} != :tag_name
    """.format(tablename=tablename, id_col=id_col, parent_col=parent_col, tag_col=tag_col)
    with sqlite3.connect(dbpath) as conn:
        cur = conn.cursor()
        cur.execute(query, {'tag_name': tag_name})
        for item in cur.fetchall():
            yield item[0]


def tag_synonyms(tag_name, dbpath, tablename='tag_synonyms', tag_col='pref_label',
                 synonym_col='alt_labels', synonym_sep='|'):
    """Yield each synonym for the given tag."""
    query = """select {synonym_col}
    from {tablename}
    where {tag_col} = :tag_name""".format(tablename=tablename, tag_col=tag_col,
                                          synonym_col=synonym_col)
    with sqlite3.connect(dbpath) as conn:
        cur = conn.cursor()
        cur.execute(query, {'tag_name': tag_name})
        for item in cur.fetchall():
            for synonym in item[0].split(synonym_sep):
                yield synonym


def person_display_name(quoted_name, dbpath, tablename='person', name_col='display_name'):
    """Return the display name for the given name after trying to fix some common mispells."""
    quoted_name = quoted_name.strip().casefold().replace('-', ' ').replace('_', ' ')
    quoted_name = ' '.join(quoted_name.split())
    query = """select {name_col}
from {tablename}
where replace(replace(lower({name_col}), '-', ' '), '_', ' ') = :quoted_name""".format(
        tablename=tablename, name_col=name_col
    )
    with sqlite3.connect(dbpath) as conn:
        cur = conn.cursor()
        cur.execute(query, {'quoted_name': quoted_name})
        res = cur.fetchall()
    if res:
        return res[0][0]
    else:
        return ''


def person_similar_names(quoted_name, dbpath, tablename='person', name_col='display_name'):
    """Return at most 3 names similar to the given name."""
    quoted_name = quoted_name.strip()
    quoted_name = ' '.join(quoted_name.split())
    query = 'select {name_col} from {tablename}'.format(tablename=tablename, name_col=name_col)
    with sqlite3.connect(dbpath) as conn:
        cur = conn.cursor()
        cur.execute(query)
        available_names = [item[0] for item in cur.fetchall()]
    return difflib.get_close_matches(quoted_name, available_names)


def taxon_synonyms(quoted_name, dbpath, tablename='taxon_synonyms', name_col='quoted_name',
                   sciname_col='sciname', scinames_col='scinames', cnames_col='cnames'):
    """Yield other names for the given taxon name after trying to fix some common mispells."""
    quoted_name = quoted_name.strip().casefold()
    quoted_name = ' '.join(quoted_name.split())
    query = """  select {sciname_col}, printf('%s|%s', {scinames_col}, {cnames_col}) as synonyms
  from {tablename}
  where lower({name_col}) = :quoted_name
union
  select {sciname_col}, printf('%s|%s', {scinames_col}, {cnames_col}) as synonyms
  from {tablename}
  where lower({sciname_col}) = :quoted_name
union
  select {sciname_col}, printf('%s|%s', {scinames_col}, {cnames_col}) as synonyms
  from {tablename}
  where lower(printf('%s [%s]', {name_col}, {sciname_col})) = :quoted_name""".format(
        tablename=tablename, name_col=name_col, sciname_col=sciname_col, scinames_col=scinames_col,
        cnames_col=cnames_col
    )
    with sqlite3.connect(dbpath) as conn:
        cur = conn.cursor()
        cur.execute(query, {'quoted_name': quoted_name})
        res = cur.fetchall()
    if len(res) == 1:
        sciname, other_names = res[0]
        return sciname, set(filter(lambda s: s.strip(), other_names.split('|')))
    else:
        return '', set()


def taxon_similar_names(quoted_name, dbpath, tablename='taxon_synonyms', name_col='quoted_name',
                        sciname_col='sciname'):
    """Return at most 3 taxon names similar to the given name."""
    quoted_name = quoted_name.strip()
    quoted_name = ' '.join(quoted_name.split())
    query = """select quoted_name, printf('%s [%s]', {name_col}, {sciname_col}) as match_name
from {tablename}""".format(tablename=tablename, name_col=name_col, sciname_col=sciname_col)
    with sqlite3.connect(dbpath) as conn:
        cur = conn.cursor()
        cur.execute(query)
        available_names = cur.fetchall()
    close_matches = tuple(difflib.get_close_matches(
        quoted_name,
        [quoted_name for quoted_name, _ in available_names]
    ))
    return [match_name for quoted_name, match_name in available_names
            if quoted_name in close_matches]
