"""Tools to process pictures."""

from collections.abc import Callable
from contextlib import contextmanager
import datetime
import logging
import pathlib
import shutil

from ana_digikam_script.exiftool import (
    append_to_meta_property,
    has_meta_tag,
    json_metadata,
    set_meta_property,
)
from ana_digikam_script.dbqueries import (
    parent_tags,
    person_display_name,
    person_similar_names,
    tag_synonyms,
    taxon_similar_names,
    taxon_synonyms,
)


class ProcessPicture(Callable):
    """Callable that process a picture."""

    def __init__(self, config):
        dbfolder = pathlib.Path(config['digikam_dbfolder'])
        self.digikam_dbpath = dbfolder.joinpath(pathlib.Path('digikam4.db'))
        self.dbpath = dbfolder.joinpath(pathlib.Path('ana-digikam-script.db'))
        self.problems = []
        self.author = ''
        self.caption_fragments = None

    def __call__(self, inpath, outpath):
        logging.debug('Processing from {} to {}'.format(inpath, outpath))
        with _transactional_copy(inpath, outpath):
            self.caption_fragments = []
            self._extract_original_caption(outpath)
            self._check_mandatory_properties(outpath)
            self._check_has_parent_tags(outpath)
            self._set_author(outpath)
            self._add_taxon_synonyms(outpath)
            self._add_tag_synonyms(outpath)
            if self.problems:
                self._report_problems(inpath)
                raise RuntimeError('Problems during process')
            else:
                self._update_metadata(outpath)
            self.caption_fragments = None

    def _extract_original_caption(self, imgpath):
        logging.debug('Extracting original caption...')
        md = json_metadata(imgpath)
        caption = md.get('IPTC:Caption-Abstract') or ''
        caption = next((item.strip() for item in caption.split('|')))
        if caption:
            self.caption_fragments.append(caption)

    def _check_mandatory_properties(self, imgpath):
        logging.debug('Checking for mandatory metadata...')
        if not has_meta_tag(imgpath, 'TITLE'):
            self.problems.append('Titre manquant')
        if not has_meta_tag(imgpath, 'TAGS'):
            self.problems.append('Aucune étiquette attribuée')
        if not has_meta_tag(imgpath, 'COPYRIGHT'):
            self.problems.append('Licence manquante')

    def _check_has_parent_tags(self, imgpath):
        logging.debug('Checking for complete tag hierarchy...')
        md = json_metadata(imgpath)
        iptc_tags = md.get('IPTC:Keywords') or []
        xmp_tags = md.get('XMP:Subject') or []
        iptc_tags = {iptc_tags} if isinstance(iptc_tags, str) else set(iptc_tags)
        xmp_tags = {xmp_tags} if isinstance(xmp_tags, str) else set(xmp_tags)
        if iptc_tags != xmp_tags:
            self.problems.append('Étiquettes XMP et IPTC différentes')
        for tag_name in iptc_tags:
            for parent_tag in parent_tags(tag_name, self.digikam_dbpath):
                if parent_tag not in iptc_tags:
                    self.problems.append(
                        'Étiquette parente « {} » manquante pour « {} »'.format(parent_tag,
                                                                                tag_name)
                    )
                    break

    def _set_author(self, imgpath):
        logging.debug('Setting author...')
        md = json_metadata(imgpath)
        caption = md.get('IPTC:Caption-Abstract') or ''
        try:
            _, quoted_author = next((item.strip() for item in caption.split('|')
                                     if item.strip().casefold().startswith('auteur'))).split(':')
        except StopIteration:
            self.problems.append('Auteur manquant dans la légende')
            return
        else:
            quoted_author = quoted_author.strip()
            self.author = person_display_name(quoted_author, self.dbpath)
        if not self.author:
            suggestions = person_similar_names(quoted_author, self.dbpath)
            self.problems.append(
                'Auteur « {author} » introuvable. '
                'Noms approchants : {suggestions}'.format(author=quoted_author,
                                                          suggestions=' ; '.join(suggestions)))

    def _add_tag_synonyms(self, imgpath):
        logging.debug('Adding tag synonyms...')
        md = json_metadata(imgpath)
        tags = md.get('IPTC:Keywords') or []
        tags = {tags} if isinstance(tags, str) else set(tags)
        synonyms = set()
        for tag in tags:
            for synonym in tag_synonyms(tag, self.dbpath):
                if synonym not in tags:
                    synonyms.add(synonym)
        if synonyms:
            self.caption_fragments.append(
                'Autre(s) mot(s)-clé(s) : {}'.format(' ; '.join(synonyms))
            )

    def _add_taxon_synonyms(self, imgpath):
        logging.debug('Adding taxon synonyms...')
        md = json_metadata(imgpath)
        caption = md.get('IPTC:Caption-Abstract') or ''
        caption_fragments = []
        for taxon_info in (item.strip() for item in caption.split('|')
                           if item.strip().casefold().startswith('taxon')):
            _, quoted_name = taxon_info.split(':')
            quoted_name = quoted_name.strip()
            sciname, other_names = taxon_synonyms(quoted_name, self.dbpath)
            if not sciname:
                suggestions = taxon_similar_names(quoted_name, self.dbpath)
                self.problems.append(
                    'Taxon « {name} » introuvable ou ambigu. '
                    'Noms approchants : {suggestions}'.format(name=quoted_name,
                                                              suggestions=' ; '.join(suggestions)))
            else:
                if not other_names:
                    fragment = '- {sciname}'.format(sciname=sciname)
                else:
                    fragment = ('- {sciname} (autre(s) nom(s) : {other_names})'.format(
                        sciname=sciname,
                        other_names=' ; '.join(other_names)
                    ))
                caption_fragments.append(fragment)
        if caption_fragments:
            self.caption_fragments.append(
                'Taxon(s) visible(s) :\n{}'.format('\n'.join(caption_fragments))
            )

    def _report_problems(self, inpath):
        """Report found problems during processing by adding them to image caption."""
        logging.debug('Problems were found during process. Reporting in image caption...')
        for problem in self.problems:
            logging.error(problem)
        report = "Rapport d'erreurs ({now}):\n{errors}".format(
            now=datetime.datetime.now().strftime('%d/%m %H:%M'),
            errors='\n'.join('- {}'.format(problem) for problem in self.problems)
        )
        append_to_meta_property(inpath, 'CAPTION', report, sep='|')

    def _update_metadata(self, outpath):
        """Update output picture metadata."""
        logging.debug('Writing metadata...')
        if self.author:
            set_meta_property(outpath, 'AUTHOR', self.author)
        if self.caption_fragments:
            set_meta_property(outpath, 'CAPTION', '\n\n'.join(self.caption_fragments))


@contextmanager
def _transactional_copy(srcpath, dstpath):
    srcpath = pathlib.Path(srcpath)
    dstpath = pathlib.Path(dstpath)
    shutil.copyfile(srcpath, dstpath)
    try:
        yield
    except Exception:
        dstpath.unlink()
        raise
