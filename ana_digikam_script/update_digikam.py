"""Tools to update digikam data."""

from collections.abc import Callable
from contextlib import contextmanager
from urllib.parse import urljoin
from urllib.request import urlopen
import csv
import io
import logging
import os
import pathlib
import platform
import shutil
import sqlite3
import subprocess
import tarfile
import tempfile


DIGIKAM_APP_FOLDER = None


class UpdateDigikam(Callable):
    """Callable that updates Digikam data with latest data."""

    def __init__(self, config):
        self.digikam_dbpath = (pathlib.Path(config['digikam_dbfolder'])
                               .joinpath(pathlib.Path('digikam4.db')))
        self.dbpath = (pathlib.Path(config['digikam_dbfolder'])
                       .joinpath(pathlib.Path('ana-digikam-script.db')))
        self.data_repo_url = config['digikam_data_repo_url']

    def __call__(self):
        self._update_tags()
        self._update_license_templates()
        self._update_persons()
        self._update_tag_synonyms()
        self._update_taxon_synonyms()

    def _update_license_templates(self):
        if DIGIKAM_APP_FOLDER is None:
            raise RuntimeError("Cannot find digikam application folder")
        download_url = urljoin(self.data_repo_url, 'template.xml')
        logging.info('Replacing template.xml with new version ')
        logging.debug('Downloading {download_url} ...'.format(download_url=download_url))
        _download_file(download_url, DIGIKAM_APP_FOLDER.joinpath('template.xml'))

    def _update_persons(self):
        download_url = urljoin(self.data_repo_url, 'person.csv')
        logging.info('Replacing persons in {dbpath} with those in '
                     '{download_url} ...'.format(dbpath=self.dbpath, download_url=download_url))
        logging.debug('Downloading {download_url} ...'.format(download_url=download_url))
        with _download_temp_file(download_url, suffix='.csv') as tmpf:
            reader = csv.reader(tmpf, delimiter=',')
            rows = list(reader)
            with sqlite3.connect(self.dbpath) as conn:
                cur = conn.cursor()
                cur.execute('drop table if exists person')
                cur.execute('create table person (id int primary key, display_name text not null)')
                cur.executemany('insert into person (id, display_name) values (?, ?)', rows)
            with sqlite3.connect(self.dbpath, isolation_level=None) as conn:
                conn.execute("VACUUM")

    def _update_tag_synonyms(self):
        download_url = urljoin(self.data_repo_url, 'tag_synonyms.csv')
        logging.info('Replacing tag synonyms in {dbpath} with those in '
                     '{download_url} ...'.format(dbpath=self.dbpath, download_url=download_url))
        logging.debug('Downloading {download_url} ...'.format(download_url=download_url))
        with _download_temp_file(download_url, suffix='.csv') as tmpf:
            reader = csv.reader(tmpf, delimiter=',')
            rows = list(reader)
            with sqlite3.connect(self.dbpath) as conn:
                cur = conn.cursor()
                cur.execute('drop table if exists tag_synonyms')
                cur.execute('create table tag_synonyms (pref_label text not null, '
                            'alt_labels text not null)')
                cur.executemany('insert into tag_synonyms (pref_label, alt_labels) values (?, ?)',
                                rows)
            with sqlite3.connect(self.dbpath, isolation_level=None) as conn:
                conn.execute("VACUUM")

    def _update_tags(self):
        download_url = urljoin(self.data_repo_url, 'tags.csv')
        logging.info('Replacing tags in {dbpath} with those in '
                     '{download_url} ...'.format(dbpath=self.digikam_dbpath,
                                                 download_url=download_url))
        logging.debug('Downloading {download_url} ...'.format(download_url=download_url))
        with _download_temp_file(download_url, suffix='.csv') as tmpf:
            reader = csv.reader(tmpf, delimiter=',')
            rows = list(reader)
            with sqlite3.connect(self.digikam_dbpath) as conn:
                cur = conn.cursor()
                cur.execute("delete from tags where id != 1 and pid != 1")
                cur.executemany('insert into tags (id, pid, name) values (?, ?, ?)', rows)
            with sqlite3.connect(self.digikam_dbpath, isolation_level=None) as conn:
                conn.execute("VACUUM")

    def _update_taxon_synonyms(self):
        download_url = urljoin(self.data_repo_url, 'taxon_synonyms.tar.gz')
        logging.info('Replacing taxon synonyms in {dbpath} with those in '
                     '{download_url} ...'.format(dbpath=self.dbpath, download_url=download_url))
        logging.debug('Downloading {download_url} ...'.format(download_url=download_url))
        with _download_temp_binary_file(download_url, suffix='.tar.gz') as downf:
            if platform.system() == 'Windows':
                powershell_path = shutil.which('Powershell')
                if not powershell_path:
                    logging.warning('Cannot find Powershell to unblock file. '
                                    'Windows may block dowloaded files. In that case you may '
                                    'tell Windows to not block files from the internet using '
                                    'registry.')
                else:
                    subprocess.run([powershell_path, 'Unblock-File', '-Path', downf.name])
            downf.seek(0)
            with tarfile.open(fileobj=downf) as tarf, \
                    tarf.extractfile('taxon_synonyms.csv') as tmpbf, \
                    io.TextIOWrapper(tmpbf, encoding='utf-8', newline='') as tmpf:
                reader = csv.reader(tmpf, delimiter=',')
                rows = list(reader)
                with sqlite3.connect(self.dbpath) as conn:
                    cur = conn.cursor()
                    cur.execute('drop table if exists taxon_synonyms')
                    cur.execute('create table taxon_synonyms (quoted_name text not null, '
                                'sciname text not null, scinames text, cnames text)')
                    cur.executemany('insert into taxon_synonyms '
                                    '(quoted_name, sciname, scinames, cnames) values (?, ?, ?, ?)',
                                    rows)
                with sqlite3.connect(self.dbpath, isolation_level=None) as conn:
                    conn.execute("VACUUM")


def find_digikam_app_folder(config):
    """Return the path to digiKam application folder."""
    global DIGIKAM_APP_FOLDER
    DIGIKAM_APP_FOLDER = config.get('digikam_app_folder')
    if not DIGIKAM_APP_FOLDER:
        DIGIKAM_APP_FOLDER = {
            'Linux': _linux_app_folder,
            'Windows': _windows_app_folder,
        }[platform.system()]().joinpath('digikam')


def _download_file(url, dstpath):
    """Download file at given URL into given file which will be overwritten."""
    dstpath = pathlib.Path(dstpath)
    with urlopen(url) as f:
        data = f.read()
        dstpath.write_text(data.decode('utf-8'), encoding='utf-8')


@contextmanager
def _download_temp_file(url, suffix=None):
    """Download file at given URL into a temporary file."""
    with tempfile.NamedTemporaryFile(suffix=suffix) as tmpbf, \
            io.TextIOWrapper(tmpbf, encoding='utf-8', newline='') as tmpf:
        with urlopen(url) as f:
            data = f.read()
            tmpf.write(data.decode('utf-8'))
        tmpf.seek(0)
        yield tmpf


@contextmanager
def _download_temp_binary_file(url, suffix=None):
    """Download file at given URL into a temporary file."""
    with tempfile.NamedTemporaryFile(suffix=suffix) as tmpf:
        with urlopen(url) as f:
            tmpf.write(f.read())
        tmpf.seek(0)
        yield tmpf


def _linux_app_folder():
    """Return the path to application foler for a Linux system."""
    app_folder = os.getenv('XDG_DATA_HOME')
    if app_folder:
        return pathlib.Path(app_folder)
    else:
        return pathlib.Path.home().joinpath('.local', 'share')


def _windows_app_folder():
    """Return the path to application foler for a Windows system."""
    app_folder = os.getenv('LOCALAPPDATA')
    if app_folder:
        return pathlib.Path(app_folder)
    else:
        return pathlib.Path.home().joinpath('AppData', 'Local')
