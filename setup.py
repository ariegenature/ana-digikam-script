""" Setup script for the ana_digikam_script application."""

from pathlib import Path

from setuptools import find_packages, setup


_config = {
    'name': 'ana_digikam_script',
    'url': 'https://framagit.org/ariegenature/ana-digikam-script',
    'author': 'Yann Voté',
    'author_email': 'ygversil@lilo.org',
    'packages': find_packages(),
    'install_requires': [
    ],
    'setup_requires': [
        'pytest-runner',
    ],
    'tests_require': [
        'pytest',
        'tox',
    ],
    'extras_require': {
        'dev': [
            'Sphinx',
            'bumpversion',
            'flake8',
            'pytest',
            'tox',
        ],
    },
    'entry_points': {
        'console_scripts': ('ana-digikam-script = ana_digikam_script.cli:main',),
    },
    'data_files': [
        ('examples', [
            str(Path('etc').joinpath('ana-digikam-script.ini.example'))
        ]),
    ],
}


def _version():
    """Get the local package version."""
    namespace = {}
    path = Path(_config["name"], "__version__.py")
    exec(path.read_text(), namespace)
    return namespace["__version__"]


def main() -> int:
    """Setup the application."""
    _config.update({
        'version': _version(),
    })
    setup(**_config)
    return 0


if __name__ == "__main__":
    raise SystemExit(main())
