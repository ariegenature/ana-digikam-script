""" Test suite for the cli module.

The script can be executed on its own or incorporated into a larger test suite.
However the tests are run, be aware of which version of the module is actually
being tested. If the library is installed in site-packages, that version takes
precedence over the version in this project directory. Use a virtualenv test
environment or setuptools develop mode to test against the development version.
"""

from subprocess import call
from sys import executable

import pytest


@pytest.fixture(params=("hello",))
def command(request):
    """Return the command to run."""
    return request.param


def test_script(command):
    """Test command line execution."""
    # Call with the --help option as a basic sanity check.
    cmdl = "{:s} -m ana_digikam_script.cli {:s} --help".format(executable, command)
    assert 0 == call(cmdl.split())
    return


if __name__ == "__main__":
    raise SystemExit(pytest.main([__file__]))
